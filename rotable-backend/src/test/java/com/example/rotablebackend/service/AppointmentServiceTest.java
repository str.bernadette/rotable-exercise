package com.example.rotablebackend.service;

import com.example.rotablebackend.*;
import org.apache.tomcat.jni.Local;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Stream;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class AppointmentServiceTest {

    @MockBean
    UserService userService;

    @MockBean
    AppointmentRepository appointmentRepository;

    @Autowired
    AppointmentService appointmentService;


    static Stream<Arguments> params(){
        return Stream.of(
                Arguments.of(
                        LocalDateTime.of(2020,1,1,1,30),
                        LocalDateTime.of(2020,1,1,2,30),
                        LocalDateTime.of(2020,1,1,1,0),
                        LocalDateTime.of(2020,1,1,2,0),
                        true
                ),
                Arguments.of(
                        LocalDateTime.of(2020,1,1,1,30),
                        LocalDateTime.of(2020,1,1,2,30),
                        LocalDateTime.of(2020,1,1,2,0),
                        LocalDateTime.of(2020,1,1,3,30),
                        true
                ),
                Arguments.of(
                        LocalDateTime.of(2020,1,1,1,30),
                        LocalDateTime.of(2020,1,1,2,30),
                        LocalDateTime.of(2020,1,1,3,30),
                        LocalDateTime.of(2020,1,1,4,30),
                        false
                )
        );
    }

    @ParameterizedTest
    @MethodSource("params")
    void testOverlappingCheck(LocalDateTime inputStart, LocalDateTime inputEnd,
                              LocalDateTime overlapStart, LocalDateTime overlapEnd,
                              Boolean expected){
        User user = new User("test","test","test", Set.of("ROLE_USER"));
        user.setId(1L);

        Appointment input = new Appointment(inputStart, inputEnd, "test", Set.of(user));

        Appointment overlapping = new Appointment(overlapStart, overlapEnd, "test2", Set.of(user));
        overlapping.setId(1L);

        Mockito.when(appointmentRepository.findByUsersContaining(user)).thenReturn(Set.of(overlapping));

        boolean isOverlapping = appointmentService.isOverlapping(input);

        Assertions.assertEquals(isOverlapping, expected);
    }


}
