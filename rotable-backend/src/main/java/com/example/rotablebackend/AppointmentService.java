package com.example.rotablebackend;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final UserService userService;

    public AppointmentService(AppointmentRepository appointmentRepository, UserService userService) {
        this.appointmentRepository = appointmentRepository;
        this.userService = userService;
    }

    public Appointment save(Appointment appointment, List<Long> userIds){
        // avoid having a large JSON for the input by fetching the User objects here
        Set<User> users = userIds.stream()
                .map(id -> userService.getById(id))
                .collect(Collectors.toSet());
        appointment.setUsers(users);

        if(isOverlapping(appointment)){
            return null;
        }
        return appointmentRepository.save(appointment);
    }

    public Appointment update(Appointment appointment, Long id){
        Optional<Appointment> oAppointment = appointmentRepository.findById(id);

        if(oAppointment.isPresent()){
            Appointment toUpdate =  oAppointment.get();
            if(appointment.getStartTime()!=null){
                toUpdate.setStartTime(appointment.getStartTime());
            }
            if(appointment.getEndTime()!=null){
                toUpdate.setEndTime(appointment.getEndTime());
            }
            if(appointment.getTitle()!=null){
                toUpdate.setTitle(appointment.getTitle());
            }
            if(isOverlapping(appointment)){
                return null;
            }
            return appointmentRepository.save(toUpdate);
        }
        return null;
    }

    public void delete(Long id){
        if(appointmentRepository.existsById(id)){
            appointmentRepository.deleteById(id);
        }
    }

    public List<Appointment> getAll(){
        return appointmentRepository.findAll();
    }

    public Appointment getOne(Long id){
        return appointmentRepository.findById(id)
                .orElse(null);
    }

    public Set<Appointment> getByUser(Long id){
        User user = userService.getById(id);
        if(user!=null){
            return appointmentRepository.findByUsersContaining(user);
        }
        return Collections.emptySet();
    }

    public List<Appointment> getByTitle(String title){
        return appointmentRepository.findByTitleContaining(title);
    }

    public boolean isOverlapping(Appointment appointment){
        Set<User> users = appointment.getUsers();
        Set<Appointment> appointments = users.stream()
                .map(user -> appointmentRepository.findByUsersContaining(user))
                // flatten due to Stream of Sets
                .flatMap(Collection::stream)
                // removes duplicates
                .collect(Collectors.toSet());

        return appointments.stream()
                .anyMatch(app -> app.getStartTime().isBefore(appointment.getEndTime()) &&
                        appointment.getStartTime().isBefore(app.getEndTime()));

    }

}
