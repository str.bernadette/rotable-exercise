package com.example.rotablebackend;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    Set<Appointment> findByUsersContaining(User user);
    List<Appointment> findByTitleContaining(String title);
}
