package com.example.rotablebackend;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Configuration
@ConfigurationProperties("schedule")
public class AdminConfiguration {

    private List<User> users;

    void setUsers(List<User> users){
        this.users = users;
    }

    @Bean
    ApplicationRunner adminUser(UserRepository userRepository, PasswordEncoder passwordEncoder){
        return args -> {
            //check if admin(developer) already in database
            for(User user: this.users){
                if (userRepository.existsByUsername(user.getUsername())){
                    continue;
                }
                String password = user.getPassword();
                String encoded = passwordEncoder.encode(password);
                user.setPassword(encoded);
                userRepository.save(user);
            }
        };
    }

}
