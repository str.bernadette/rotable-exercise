package com.example.rotablebackend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final AppointmentRepository appointmentRepository;
    private final PasswordEncoder passwordEncoder;
    private final Set<String> userAuthorities;

    public UserService(UserRepository userRepository, AppointmentRepository appointmentRepository, PasswordEncoder passwordEncoder,
                       @Value("${schedule.authorities}") Set<String> userAuthorities) {
        this.userRepository = userRepository;
        this.appointmentRepository = appointmentRepository;
        this.passwordEncoder = passwordEncoder;
        this.userAuthorities = userAuthorities;
    }

    public User save(User user){ //POST-> process(check if exists), save and return
        if (user.getId()!= null && userRepository.existsById(user.getId())){
            return userRepository.getById(user.getId());
        }
        String password = user.getPassword();
        String encoded = passwordEncoder.encode(password);
        user.setPassword(encoded);
        user.setAuthorities(userAuthorities);
        return userRepository.save(user);
    }

    public User update(User user, Long id){
        Optional<User> oUser = userRepository.findById(id);
        if(oUser.isPresent()){
            User toUpdate = oUser.get();
            if(user.getFullName()!=null){
                toUpdate.setFullName(user.getFullName());
            }
            return userRepository.save(toUpdate);
        }
        return null;
    }

    public void delete(Long id){
        Optional<User> oUser = userRepository.findById(id);
        if(oUser.isPresent()){
            User user = oUser.get();
            Set<Appointment> appointments = appointmentRepository.findByUsersContaining(user);
            for(Appointment app: appointments){
                app.getUsers().remove(user);
                appointmentRepository.save(app);
            }
            userRepository.delete(user);
        }
    }

    public List<User> getAll(){
        return userRepository.findAll();
    }

    public User getById(Long id){
        return userRepository.findById(id)
                .orElse(null);
    }
}