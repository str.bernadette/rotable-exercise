package com.example.rotablebackend;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@Secured("ROLE_DEVELOPER")
public class UserEndpoint {

    private final UserService userService;

    public UserEndpoint(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    List<User> getAll(){
        return userService.getAll();
    }

    @GetMapping("/{id}")
    User getById(@PathVariable Long id){
        return userService.getById(id);
    }

    @PostMapping
    User save(@RequestBody User user){
        return userService.save(user);
    }

    @PutMapping("/{id}")
    User update(@RequestBody User user, @PathVariable Long id){
        return userService.update(user,id);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id){
        userService.delete(id);
    }




}
