package com.example.rotablebackend;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/appointments")
@CrossOrigin(origins = "http://localhost:4200")
@Secured("ROLE_USER")
public class AppointmentEndpoint {

    private final AppointmentService appointmentService;

    public AppointmentEndpoint(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @GetMapping
    List<Appointment> getAll(){
        return appointmentService.getAll();
    }

    @GetMapping("/{id}")
    Appointment getById(@PathVariable Long id){
        return appointmentService.getOne(id);
    }

    @GetMapping("/user/{id}")
    Set<Appointment> getByUserId(@PathVariable Long id){
        return appointmentService.getByUser(id);
    }

    @GetMapping("/search")
    List<Appointment> getByTitle(@RequestParam String title){
        return appointmentService.getByTitle(title);
    }

    @PostMapping
    Appointment save(@RequestBody Appointment appointment, @RequestParam List<Long> userIds){
        return appointmentService.save(appointment, userIds);
    }

    @PutMapping("/{id}")
    Appointment update(@RequestBody Appointment appointment, @PathVariable Long id){
        return appointmentService.update(appointment, id);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id){
        appointmentService.delete(id);
    }


}
