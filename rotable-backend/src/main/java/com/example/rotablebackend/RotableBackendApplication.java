package com.example.rotablebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RotableBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(RotableBackendApplication.class, args);
    }

}
