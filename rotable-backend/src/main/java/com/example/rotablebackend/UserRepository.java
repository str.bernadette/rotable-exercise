package com.example.rotablebackend;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    //used in SecurityConfig
    Optional<User> findOneByUsername(String username);

    //used in AdminConfig to check if admin(developer) exists
    boolean existsByUsername(String username);

}
