import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppointmentListComponent} from "./appointment-list/appointment-list.component";
import {AppointmentDetailComponent} from "./appointment-detail/appointment-detail.component";

const routes: Routes = [
  { path: 'appointments', component: AppointmentListComponent },
  { path: '', redirectTo: '/appointments', pathMatch: 'full' },
  { path: 'detail/:id', component: AppointmentDetailComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
