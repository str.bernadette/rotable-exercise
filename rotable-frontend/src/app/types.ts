export interface Appointment {
  id: number;
  startTime: number;
  endTime: number;
  title: string;
  users: User[];
}

export interface User {
  id: number;
  username: string;
  fullName: string;
  authorities: string[];
}
