import { Component, OnInit } from '@angular/core';
import {Appointment} from "../types";
import {AppointmentService} from "../appointment.service";

@Component({
  selector: 'app-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.css']
})
export class AppointmentListComponent implements OnInit {

  appointments: Appointment[] = [];

  constructor(private appointmentService:AppointmentService) { }

  ngOnInit(): void {
    this.getAppointments();
  }

  getAppointments(): void {
    this.appointmentService.getAppointments()
      .subscribe(appointments => this.appointments = appointments);
  }


  delete(appointment: Appointment): void {
    this.appointments = this.appointments.filter(h => h !== appointment);
    this.appointmentService.deleteAppointment(appointment.id).subscribe();
  }

}
