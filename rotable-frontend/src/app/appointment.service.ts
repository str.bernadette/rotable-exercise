import { Injectable } from '@angular/core';
import {Appointment} from "./types";
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private messageService: MessageService,
              private http: HttpClient) {}

  private appointmentsUrl = 'http://localhost:8080/appointments';
  //The appointments web API expects a special header in HTTP save requests

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Basic ' + btoa('user1:user1')
    })
  };

  /** GET appointment from the server */
  getAppointments(): Observable<Appointment[]> {
    return this.http.get<Appointment[]>(this.appointmentsUrl, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched appointments')),
        catchError(this.handleError<Appointment[]>('getAppointments', []))
      );
  }

  /** GET appointment by id. Will 404 if id not found */
  getAppointment(id: number): Observable<Appointment> {
    const url = `${this.appointmentsUrl}/${id}`;
    return this.http.get<Appointment>(url, this.httpOptions)
      .pipe(
        tap(_ => this.log(`fetched appointment id=${id}`)),
        catchError(this.handleError<Appointment>(`getAppointment id=${id}`))
      );
  }

  /** DELETE: delete the appointment from the server */
  deleteAppointment(id: number): Observable<Appointment> {
    const url = `${this.appointmentsUrl}/${id}`;
    return this.http.delete<Appointment>(url, this.httpOptions)
      .pipe(
        tap(_ => this.log(`deleted appointment id=${id}`)),
        catchError(this.handleError<Appointment>('deleteAppointment'))
      );
  }

  /* GET appointments whose name contains search term */
  searchAppointments(term: string): Observable<Appointment[]> {
    if (!term.trim()) {
      // if not search term, return empty appointment array.
      return of([]);
    }
    return this.http.get<Appointment[]>(`${this.appointmentsUrl}/search?title=${term}`, this.httpOptions).pipe(
      tap(x => x.length ?
        this.log(`found appointments matching "${term}"`) :
        this.log(`no appointments matching "${term}"`)),
      catchError(this.handleError<Appointment[]>('searchAppointments', []))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log an AppointmentService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`AppointmentService: ${message}`);
  }
}
