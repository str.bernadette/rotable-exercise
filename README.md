# Database
## Setup

Create a MySQL database with the following command:

```sql
CREATE DATABASE `rotable-schedule`;
```

# Backend

A power-user with username developer is provided to create other users.

## Installation

```bash
mvn clean compile
```

## Running
Run with the IDE of choice


# Frontend
A user with username user1 is hardcoded in the frontend REST calls for ease of use.

## Installation

```bash
npm install
```
## Running
Run with

```bash
ng serve
```


## License
[MIT](https://choosealicense.com/licenses/mit/)